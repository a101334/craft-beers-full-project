<?php 
    require_once __DIR__ . '/../model/beerDAO.php';

    class BeerController {
        
        public function getAllBeers () {

            try {
                $beerDAO = new BeerDAO();
                $reponse = [
                    'statusCode' => 200,
                    'body' => $beerDAO->getAll()
                ];
                return $reponse;
            } catch (Exception $e) {
                $reponse = [ 'statusCode' => 500 ];
                return $reponse;
            }
        }

        public function getBeer ($id) {

            try {
                $beerDAO = new BeerDAO();
                $reponse = [
                    'statusCode' => 200,
                    'body' => $beerDAO->getId($id)
                ];
                return $reponse;
            } catch (Exception $e) {
                $reponse = [ 'statusCode' => 500 ];
                return $reponse;
            }
        }

        public function saveBeer ($body) {

            try {

                try {
                    if ($this->isValidBody($body)) {
                        $beerDAO = new BeerDAO();
                    
                        $reponse = [
                            'statusCode' => 201,
                            'body' => $beerDAO->save($body)
                        ];
                        return $reponse;
                    }
                    
                    $reponse = [
                        'statusCode' => 405,
                        'body' => ['message' => 'Invalid input']
                    ];
                    return $reponse;
                } catch (Exception $e) {
                    $reponse = [ 'statusCode' => 500 ];
                    return $reponse;
                }
            } catch (Exception $e) {
                $reponse = [ 'statusCode' => 500 ];
                    return $reponse;
            }

            
        }

        public function updateBeer ($body, $id) {

            try {

                try {
                    if ($this->isValidBody($body)) {
                        $beerDAO = new BeerDAO();
                    
                        $reponse = [
                            'statusCode' => 200,
                            'body' => $beerDAO->update($body, $id)
                        ];
                        return $reponse;
                    }
                    
                    $reponse = [
                        'statusCode' => 405,
                        'body' => ['message' => 'Invalid input']
                    ];
                    return $reponse;
                } catch (Exception $e) {
                    $reponse = [ 'statusCode' => 500 ];
                    return $reponse;
                }
            } catch (Exception $e) {
                $reponse = [ 'statusCode' => 500 ];
                    return $reponse;
            }

            
        }

        public function pathBeer ($body, $id) {

            try {

                try {
                    if ($this->isValidBodyPath($body)) {
                        $beerDAO = new BeerDAO();
                    
                        $reponse = [
                            'statusCode' => 200,
                            'body' => $beerDAO->path($body, $id)
                        ];
                        return $reponse;
                    }
                    
                    $reponse = [
                        'statusCode' => 405,
                        'body' => ['message' => 'Invalid input']
                    ];
                    return $reponse;
                } catch (Exception $e) {
                    $reponse = [ 'statusCode' => 500 ];
                    return $reponse;
                }
            } catch (Exception $e) {
                $reponse = [ 'statusCode' => 500 ];
                    return $reponse;
            }

            
        }

        public function deleteBeer ($id) {

            try {
                $beerDAO = new BeerDAO();
                $beerDAO->delete($id);
                $reponse = [
                    'statusCode' => 200
                ];
                return $reponse;
            } catch (Exception $e) {
                $reponse = [ 'statusCode' => 500 ];
                return $reponse;
            }
        }

        private function isValidBody ($bodyArrayTocompare) {
            $maskArray = [
                'name' => '',
                'ingredients' => '',
                'alcoholContent' => '',
                'price' => 0,
                'category' => ''
            ];

            $diffKey = array_diff_key($maskArray, $bodyArrayTocompare);

            if ( count($diffKey) == 0 ){
                return true;
            } 

            return false;
        }

        private function isValidBodyPath ($bodyPathArrayTocompare) {
            $maskArray = [
                'name' => '',
                'ingredients' => '',
                'alcoholContent' => '',
                'price' => 0,
                'category' => ''
            ];

            foreach($bodyPathArrayTocompare as $key => $val) {
                if(!array_key_exists($key, $maskArray)) {
                    return false;
                }
            }

            return true;
        }

    }

?>