<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/controller/beerController.php';

$app = new \Slim\App;

$app->get('/beers', function (Request $request, Response $response, array $args) {

    $beerController = new BeerController();
    $res = $beerController->getAllBeers();
    $response->getBody()->write(json_encode($res['body']));
    return $response->withHeader('Content-type', 'application/json')->withStatus($res['statusCode']);
});

$app->get('/beers/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $beerController = new BeerController();
    $res = $beerController->getBeer($id);
    $response->getBody()->write(json_encode($res['body']));
    return $response->withHeader('Content-type', 'application/json')->withStatus($res['statusCode']);
});

$app->post('/beers', function (Request $request, Response $response, array $args) {
    $body = $request->getParsedBody();
    $beerController = new BeerController();
    $res = $beerController->saveBeer($body);
    
    return $response->withStatus($res['statusCode']);
});

$app->put('/beers/{id}', function (Request $request, Response $response, array $args) {

    $body = $request->getParsedBody();
    $id = $args['id'];
    $beerController = new BeerController();
    $res = $beerController->updateBeer($body, $id);
    
    return $response->withStatus($res['statusCode']);
});

$app->patch('/beers/{id}', function (Request $request, Response $response, array $args) {

    $body = $request->getParsedBody();
    $id = $args['id'];
    $beerController = new BeerController();
    $res = $beerController->pathBeer($body, $id);
    
    return $response->withStatus($res['statusCode']);
});

$app->delete('/beers/{id}', function (Request $request, Response $response, array $args) {

    $id = $args['id'];
    $beerController = new BeerController();
    $res = $beerController->deleteBeer($id);
    return $response->withHeader('Content-type', 'application/json')->withStatus($res['statusCode']);
});

$app->run();

