<?php
    require_once __DIR__ . '/config.php';

    abstract class GenericDAO {
        protected $tableName;
        private $conn;

        protected function getConnection() {
            
            try {
                $this->conn = new PDO('mysql:host='.HOST_DB.';dbname='.DB_NAME, USER_DB, PASSWORD_DB);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                return $this->conn;
            } catch (PDOException $e) {
                echo 'failed connection: '.$e->getMessage();
            }
        }

        protected function closeConnection() {

            if($this->conn) {
                $this->conn = null;
            }
        }

        protected function getTableName() {
            return $this->tableName;
        }

        protected function setTableName($tableName) {
            $this->tableName = $tableName;
        }

        public function getAll() {
            $this->getConnection();
            $sqlQuery = 'SELECT * FROM '.$this->getTableName();
            $stm = $this->conn->prepare($sqlQuery);
            $stm->execute();
            $stm->setFetchMode(PDO::FETCH_ASSOC);
            $result = $stm->fetchAll();
            $this->closeConnection(); 
            return $result;
        }

        public function getId($id) {
            $this->getConnection();
            $sqlQuery = 'SELECT * FROM '.$this->getTableName().' WHERE id =:id';
            $stm = $this->conn->prepare($sqlQuery);
            $stm->bindParam(':id', $id);
            $stm->execute();
            $stm->setFetchMode(PDO::FETCH_ASSOC);
            $result = $stm->fetchAll();
            $this->closeConnection(); 
            return $result;
        }

        public function save($body) {
            $this->getConnection();
            $sqlQueryInsert = $this->buildSqlInsert($body);
            $stm = $this->conn->prepare($sqlQueryInsert);

            foreach($body as $key => $val) {
                $stm->bindValue(':'.$key, $val);
            }
            
            $stm->execute();
            $this->closeConnection();
        }

        public function update($body, $id) {
            $this->getConnection();
            $sqlQueryUpdate = $this->buildSqlUpdate($body);
            $stm = $this->conn->prepare($sqlQueryUpdate);

            foreach($body as $key => $val) {
                $stm->bindValue(':'.$key, $val);
            }
            
            $stm->bindValue(':id', $id);

            $stm->execute();
            $this->closeConnection();
        }

        public function path($body, $id) {
            $this->getConnection();
            $sqlQueryUpdate = $this->buildSqlUpdate($body);
            $stm = $this->conn->prepare($sqlQueryUpdate);

            foreach($body as $key => $val) {
                $stm->bindValue(':'.$key, $val);
            }
            
            $stm->bindValue(':id', $id);

            $stm->execute();
            $this->closeConnection();
        }

        public function delete($id) {
            $this->getConnection();
            $sqlQueryDelete = 'DELETE FROM '.$this->getTableName().' WHERE id =:id';
            $stm = $this->conn->prepare($sqlQueryDelete);
            $stm->bindParam(':id', $id);
            $stm->execute();
            $this->closeConnection();
        }

        private function buildSqlInsert($array) {
            $sqlInsert = 'INSERT INTO '.$this->getTableName().'( ';

            foreach ($array as $key => $val) {
                $sqlInsert .= $key.',';
            }

            $sqlInsert = substr($sqlInsert, 0, -1);
            $sqlInsert .= ') VALUES ( ';


            foreach($array as $key => $val) {
                $sqlInsert .= ':'.$key.',';
            }

            $sqlInsert = substr($sqlInsert, 0, -1);

            $sqlInsert .= ')';

            return $sqlInsert;
        }

        private function buildSqlUpdate($array) {
            $sqlUpdate = 'UPDATE '.$this->getTableName().' SET ';

            foreach ($array as $key => $val) {
                $sqlUpdate .= $key.'=:'.$key.',';
            }

            $sqlUpdate = substr($sqlUpdate, 0, -1);

            $sqlUpdate .= ' WHERE id = :id';
            
            return $sqlUpdate;
        }
    }
?>