<?php
    require_once __DIR__ . '/genericDAO.php';

    class BeerDAO extends GenericDAO {
        const TABLE_NAME = 'beers';

        function __construct() {
            $this->setTableName(self::TABLE_NAME);    
        }
    }

?>