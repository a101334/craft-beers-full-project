DROP DATABASE IF EXISTS craf_beers;
CREATE DATABASE craf_beers;

USE craf_beers;

DROP TABLE IF EXISTS beers;
CREATE TABLE beers (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    ingredients VARCHAR(255) NOT NULL,
    alcoholContent VARCHAR(10) NOT NULL,
    price NUMERIC(5,2) NOT NULL,
    category VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO beers (name, ingredients, alcoholContent, price, category) 
VALUES ('Bavaria', 'Cevada industrial', '10%', 2.30, 'manofaturada'),
('Cacildis', 'Cevada pura', '10%', 4.70, 'artesanal');

SELECT * FROM beers;
